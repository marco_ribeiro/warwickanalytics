# Decision Processor Application #

## How to run ? ##

### Option 1 ###

* You need JDK 8 and Maven 3 to run the application.

* Build the code using Maven with the command: `mvn clean install`.

* Move to the `batch` project folder and execute `mvn wildfly:run`

* Go to http://localhost:8080/decpro-batch/ to access the application.

### Option 2 ###

* You need JDK 8, Maven 3 and Wildfly 8.1.0 to run the application.

* Build the code using Maven with the command: `mvn clean install`.

* Run Wildfly.

* Move to the `batch` project folder and execute `mvn wildfly:deploy-only`

* Go to http://localhost:8080/decpro-batch/ to access the application.


## Hot to use ? ##

* Submit a valid csv file

* When the File `Status` is `DONE` click on the Id (first column, first table)

* Results are shown the table `File Details`


## Points to improve ##

* Unit testing

* Integrating test to coverage all batch processing

* File Validations


####Marco Ribeiro####
marco.ribeiro@gmx.com
