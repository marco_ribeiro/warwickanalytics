package com.warwick.analytics.decpro.batch.process.data;

import com.warwick.analytics.decpro.batch.process.AbstractInputFileProcess;
import com.warwick.analytics.decpro.business.DecisionBusiness;
import com.warwick.analytics.decpro.entity.DecisionFile;
import com.warwick.analytics.decpro.entity.DecisionLine;
import com.warwick.analytics.decpro.entity.FileStatus;
import com.warwick.analytics.decpro.entity.FolderType;

import javax.batch.api.chunk.ItemReader;
import javax.batch.runtime.context.JobContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.logging.Level;

import static java.util.logging.Logger.getLogger;
import static org.apache.commons.io.FileUtils.openInputStream;

/**
 * @author Marco Ribeiro
 */
@Named
public class DecisionDataItemReader extends AbstractInputFileProcess implements ItemReader {
    private FileInputStream in;
    private BufferedReader reader;

    @Inject
    private JobContext jobContext;
    @Inject
    private DecisionBusiness decisionBusiness;

    public DecisionDataItemReader() {
        in = null;
    }

    @Override
    public void open(Serializable checkpoint) throws Exception {
        getLogger(this.getClass().getName()).log(Level.INFO, "Processing file " +
                                                             getContext().getFileToProcess().getFileName());

        in = openInputStream(getContext().getFileToProcess(FolderType.FI_TMP));

        reader = new BufferedReader(new InputStreamReader(in));

        DecisionFile fileToProcess = getContext().getFileToProcess();
        fileToProcess.setFileStatus(FileStatus.PROCESSING);
        decisionBusiness.updateDecisionFile(fileToProcess);
    }

    @Override
    public void close() throws Exception {
        DecisionFile fileToProcess = getContext().getFileToProcess();
        fileToProcess.setFileStatus(FileStatus.PROCESSED);
        decisionBusiness.updateDecisionFile(fileToProcess);

        if (in != null) {
            in.close();
        }

        getLogger(this.getClass().getName()).log(Level.INFO, "Finished file " +
                                                             getContext().getFileToProcess().getFileName());
    }

    @Override
    public Object readItem() throws Exception {
        String line = reader.readLine();
        if(line != null){
            String[] columns = line.split(",");
            getLogger(this.getClass().getName()).log(Level.FINE, "Processing line "+line);
            if (!columns[0].equalsIgnoreCase("id")) {
                DecisionLine decisionLine = new DecisionLine();
                decisionLine.setDecisionId(Long.parseLong(columns[0]));
                Integer[] variables = new Integer[columns.length-2];
                for (int i = 1; i < columns.length - 1; i++) {
                    variables[i-1] = Integer.parseInt(columns[i]);
                }
                decisionLine.setVariables(variables);
                decisionLine.setDecision(columns[columns.length-1].equalsIgnoreCase("1"));

                return decisionLine;
            } else {
                throw new HeaderClassException();
            }
        }
        return null;
    }


    @Override
    public Serializable checkpointInfo() throws Exception {
        return null;
    }

}
