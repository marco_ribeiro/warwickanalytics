package com.warwick.analytics.decpro.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 * @author Marco Ribeiro
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
      @NamedQuery(name = "DecisionFile.exists",
                  query = "SELECT COUNT(df) FROM DecisionFile df " +
                          "WHERE df.url = :url AND df.lastModified = :lastModified"),
      @NamedQuery(name = "DecisionFile.findByUrl",
                  query = "SELECT df FROM DecisionFile df " +
                          "WHERE df.url = :id"),
      @NamedQuery(name = "DecisionFile.listAll",
                query = "SELECT df FROM DecisionFile df "),
})
public class DecisionFile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "decisionFileId")
    @SequenceGenerator(name = "decisionFileId",
                       sequenceName = "DECISIONFILE_ID_SEQ",
                       initialValue = 1,
                       allocationSize = 1)
    private Long id;
    private String url;
    private Long lastModified;
    private String fileName;
    private FileStatus fileStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getLastModified() {
        return lastModified;
    }

    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public FileStatus getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(FileStatus fileStatus) {
        this.fileStatus = fileStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        DecisionFile that = (DecisionFile) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "DecisionFile{" +
               "id=" + id +
               ", url='" + url + '\'' +
               ", lastModified=" + lastModified +
               ", fileName='" + fileName + '\'' +
               ", fileStatus=" + fileStatus +
               '}';
    }
}
