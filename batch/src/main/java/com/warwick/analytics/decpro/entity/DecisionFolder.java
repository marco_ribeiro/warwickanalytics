package com.warwick.analytics.decpro.entity;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.io.Serializable;

/**
 * @author Marco Ribeiro
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "DecisionFolder.listFolders",
                query = "SELECT d FROM DecisionFolder d ORDER BY d.path")
})
public class DecisionFolder implements Serializable {
    @Id
    @Enumerated
    private FolderType id;
    private String path;

    public DecisionFolder() {
    }

    public DecisionFolder(FolderType folderyType, String path) {
        this.id = folderyType;
        this.path = path;
    }

    public FolderType getId() {
        return id;
    }

    public void setId(FolderType id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DecisionFolder that = (DecisionFolder) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "DecisionFolder{" +
                "id=" + id +
                ", path='" + path + '\'' +
                '}';
    }
}
