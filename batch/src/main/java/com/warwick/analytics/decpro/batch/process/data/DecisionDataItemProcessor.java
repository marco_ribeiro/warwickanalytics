package com.warwick.analytics.decpro.batch.process.data;

import com.warwick.analytics.decpro.batch.process.AbstractInputFileProcess;
import com.warwick.analytics.decpro.entity.DecisionLine;

import javax.batch.api.chunk.ItemProcessor;
import javax.inject.Named;

/**
 * @author Marco Ribeiro
 */
@Named
public class DecisionDataItemProcessor extends AbstractInputFileProcess implements ItemProcessor {
    @Override
    public Object processItem(Object item) throws Exception {
        DecisionLine decisionLine = (DecisionLine) item;

        decisionLine.setDecisionFile(getContext().getFileToProcess());

        return decisionLine;
    }
}
