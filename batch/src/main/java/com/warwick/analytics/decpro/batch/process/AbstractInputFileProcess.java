package com.warwick.analytics.decpro.batch.process;

import com.warwick.analytics.decpro.business.DecisionBusiness;
import com.warwick.analytics.decpro.entity.DecisionFile;
import com.warwick.analytics.decpro.entity.FolderType;

import javax.annotation.PostConstruct;
import javax.batch.runtime.context.JobContext;
import javax.inject.Inject;
import java.io.File;

import static org.apache.commons.io.FileUtils.getFile;

/**
 * @author Marco Ribeiro
 */
@SuppressWarnings("CdiManagedBeanInconsistencyInspection")
public abstract class AbstractInputFileProcess {
    @Inject
    private JobContext jobContext;
    @Inject
    private DecisionBusiness decisionBusiness;

    @PostConstruct
    private void init() {
        Long fileId = Long.valueOf(jobContext.getProperties().getProperty("fileId"));

        if (jobContext.getTransientUserData() == null) {
            jobContext.setTransientUserData(new InputFileProcessContext(fileId));
        }
    }

    protected InputFileProcessContext getContext() {
        return (InputFileProcessContext) jobContext.getTransientUserData();
    }

    public class InputFileProcessContext {
        private Long fileId;

        private DecisionFile fileToProcess;

        private InputFileProcessContext(Long fileId) {
            this.fileId = fileId;
        }

        public DecisionFile getFileToProcess() {
            if (fileToProcess == null) {
                this.fileToProcess = decisionBusiness.findDecisionFileById(fileId);
            }

            return fileToProcess;
        }

        public File getFileToProcess(FolderType folderType) {
            return getFile(decisionBusiness.findDecisionFolderById(folderType).getPath() + "/" +
                    getFileToProcess().getFileName());

        }

        public File getFolder(FolderType folderType) {
            return getFile(decisionBusiness.findDecisionFolderById(folderType).getPath());
        }
    }
}
