package com.warwick.analytics.decpro.configuration;

import com.warwick.analytics.decpro.entity.FolderType;
import static com.warwick.analytics.decpro.entity.FolderType.*;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Named;
import java.util.*;

/**
 * @author Marco Ribeiro
 */
@Singleton
@Named
@SuppressWarnings("unchecked")
public class ConfigurationFactory {
    private Map<String, Object> configurations;

    @PostConstruct
    public void initConfiguration() {
        configurations = new HashMap<>();

        Map<String, List<FolderType>> folders = new HashMap<>();
        folders.put("batch/work/", Arrays.asList(FI_TMP, FO_TMP));
        folders.put("batch/files/", Arrays.asList(FI, FO, FP));

        List<String> fileExtensions = new ArrayList<>();
        fileExtensions.add("dat");

        configurations.put("batchHome", System.getProperty("user.home") + "/wwadecpro/data");
        configurations.put("folders", folders);
        configurations.put("fileExtensions", fileExtensions);
    }

    @Produces
    @Configuration
    public String getString(InjectionPoint injectionPoint) {
        return (String) configurations.get(injectionPoint.getMember().getName());
    }

    @Produces
    @Configuration
    public <E> List<E> getList(InjectionPoint injectionPoint) {
        return (List) configurations.get(injectionPoint.getMember().getName());
    }

    @Produces
    @Configuration
    public <K,V> Map<K,V> getMap(InjectionPoint injectionPoint) {
        return (Map) configurations.get(injectionPoint.getMember().getName());
    }
}
