package com.warwick.analytics.decpro.business;

import com.warwick.analytics.decpro.entity.DecisionFile;
import com.warwick.analytics.decpro.entity.DecisionFolder;
import com.warwick.analytics.decpro.entity.DecisionLine;
import com.warwick.analytics.decpro.entity.FileStatus;
import com.warwick.analytics.decpro.entity.FolderType;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;

import static java.util.logging.Logger.getLogger;

/**
 * @author Marco Ribeiro
 */
@Named
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
@ApplicationPath("/resources")
@Path("decisions")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class DecisionBusinessBean extends Application implements DecisionBusiness {

    @PersistenceContext
    protected EntityManager em;

    @Inject
    private BatchService batchService;

    @Override
    public DecisionFile findDecisionFileById(Long id) {
        return em.find(DecisionFile.class, id);
    }

    @Override
    public DecisionFolder findDecisionFolderById(FolderType id) {
        return em.find(DecisionFolder.class, id);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public DecisionFile updateDecisionFile(DecisionFile file) {
        return em.merge(file);
    }

    @Override
    public List<DecisionFile> findDecisionByUrl(String url) {
        return em.createNamedQuery("DecisionFile.findByUrl").setParameter("id", url).getResultList();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createDecisionFile(DecisionFile file) {
        em.persist(file);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createDecisionFolder(DecisionFolder folder) {
        em.persist(folder);
    }

    @Override
    public List<DecisionFolder> listDecisionFolders() {
        return em.createNamedQuery("DecisionFolder.listFolders").getResultList();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void removeDecisionLine(Long lineId) {
        DecisionLine tR = (DecisionLine) em.createNamedQuery("DecisionLine.findById").setParameter("id", lineId).getSingleResult();
        em.remove(tR);
    }


    /*

    REST API

    */

    @Override
    @GET
    @Path("lines")
    public List<DecisionLine> findByFileId(@QueryParam("fileId") Long fileId) {
        if (fileId == null) {
            return em.createNamedQuery("DecisionLine.listAll").getResultList();
        } else {
            return em.createNamedQuery("DecisionLine.findByFileId").setParameter("id", fileId).getResultList();
        }
    }

    @Override
    @GET
    @Path("files")
    public List<DecisionFile> listDecisionFiles() {
        return em.createNamedQuery("DecisionFile.listAll").getResultList();
    }

    @Override
    @POST
    @Path("/upload")
    @Consumes({MediaType.APPLICATION_OCTET_STREAM, "text/csv"})
    @Produces(MediaType.TEXT_PLAIN)
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Response postCsvFile(InputStream fileInputStream) {

        DecisionFolder decisionFolderById = findDecisionFolderById(FolderType.FI);

        //this should be done on server startup
        if (decisionFolderById == null) {
            getLogger(this.getClass().getName()).log(Level.WARNING,
                    "Batch not prepared to process input files");
        }

        try {
            File file = readInputFile(fileInputStream, decisionFolderById);
            DecisionFile decisionFile = new DecisionFile();
            decisionFile.setUrl(file.getAbsolutePath());
            decisionFile.setFileStatus(FileStatus.LOADED);
            decisionFile.setFileName(file.getName());
            createDecisionFile(decisionFile);
            getLogger(this.getClass().getName()).log(Level.INFO,
                    "DecisionFile [fileId="+decisionFile.getId()+"] created and ready to be processed");
            batchService.execute(decisionFile.getId());
            getLogger(this.getClass().getName()).log(Level.INFO,
                    "Batch service called to process DecisionFile [fileId="+decisionFile.getId()+"]");
            return Response.ok(decisionFile.getId()).build();
        } catch (IOException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }


    }

    private File readInputFile(InputStream fileInputStream, DecisionFolder decisionFolderById) throws IOException {
        int read;
        byte[] bytes = new byte[1024];
        File destinationFile = new File(decisionFolderById.getPath(), System.currentTimeMillis() + ".csv");
        OutputStream out = new FileOutputStream(destinationFile);
        int total = 0;
        while ((read = fileInputStream.read(bytes)) != -1) {
            out.write(bytes, 0, read);
            total += read;
        }
        out.flush();
        out.close();
        getLogger(this.getClass().getName()).log(Level.INFO,
                "Received file [" + destinationFile.getName() + "] and saved at " + destinationFile.getAbsolutePath() + " with size " + total);
        return destinationFile;
    }

}