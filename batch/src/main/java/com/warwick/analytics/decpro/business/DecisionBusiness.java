package com.warwick.analytics.decpro.business;

import com.warwick.analytics.decpro.entity.DecisionFile;
import com.warwick.analytics.decpro.entity.DecisionFolder;
import com.warwick.analytics.decpro.entity.DecisionLine;
import com.warwick.analytics.decpro.entity.FolderType;

import javax.ejb.Local;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.List;

/**
 * @author Marco Ribeiro
 */
@Local
public interface DecisionBusiness {
    DecisionFile findDecisionFileById(Long id);

    DecisionFolder findDecisionFolderById(FolderType id);

    DecisionFile updateDecisionFile(DecisionFile file);

    List<DecisionFile> findDecisionByUrl(String url);

    void createDecisionFile(DecisionFile file);

    void createDecisionFolder(DecisionFolder folder);

    List<DecisionFolder> listDecisionFolders();

    List<DecisionLine> findByFileId(Long fileId);

    List<DecisionFile> listDecisionFiles();

    Response postCsvFile(InputStream fileInputStream);

    void removeDecisionLine(Long lineId);

}
