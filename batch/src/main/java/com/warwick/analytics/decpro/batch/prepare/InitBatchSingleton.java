/**
 * $$Id$$
 *
 * @author mribeiro
 * @date 12/02/16 18:49
 * <p>
 * Copyright (C) 2016 MRibeiro
 * marco.lob@gmail.com
 * <p>
 * All rights reserved.
 */
package com.warwick.analytics.decpro.batch.prepare;

import com.warwick.analytics.decpro.business.BatchService;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 *
 */
@Startup
@Singleton
public class InitBatchSingleton {

    @Inject
    private BatchService batchService;

    @PostConstruct
    public void initBatch(){
        System.out.println("init post construct");
        batchService.prepare();
    }
}