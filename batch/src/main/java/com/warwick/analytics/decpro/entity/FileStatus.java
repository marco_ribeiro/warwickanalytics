package com.warwick.analytics.decpro.entity;

/**
 * @author Marco Ribeiro
 */
public enum FileStatus {
    LOADED, DOWNLOADED, PROCESSING, PROCESSED, DONE
}
