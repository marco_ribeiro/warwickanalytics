package com.warwick.analytics.decpro.business;

import javax.ejb.Local;

/**
 * @author Marco Ribeiro
 */
@Local
public interface BatchService {

    void prepare();

    void execute(long fileId);
}