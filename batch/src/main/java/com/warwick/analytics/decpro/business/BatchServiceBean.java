package com.warwick.analytics.decpro.business;

import javax.annotation.PostConstruct;
import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.ejb.Stateless;
import javax.inject.Named;
import java.util.Properties;

/**
 * @author Marco Ribeiro
 */
@Named
@Stateless
public class BatchServiceBean implements BatchService {

    @Override
    public void prepare() {
        JobOperator jobOperator = BatchRuntime.getJobOperator();
        jobOperator.start("prepare-job", new Properties());
    }

    @Override
    public void execute(long fileId) {
        Properties jobParameters = new Properties();
        JobOperator jobOperator = BatchRuntime.getJobOperator();
        jobParameters.setProperty("fileId", String.valueOf(fileId));
        jobOperator.start("process-job", jobParameters);

    }
}