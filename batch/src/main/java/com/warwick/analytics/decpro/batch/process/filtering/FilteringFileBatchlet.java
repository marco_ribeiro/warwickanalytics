package com.warwick.analytics.decpro.batch.process.filtering;

import com.warwick.analytics.decpro.batch.process.AbstractInputFileProcess;
import com.warwick.analytics.decpro.business.DecisionBusiness;
import com.warwick.analytics.decpro.entity.DecisionFile;
import com.warwick.analytics.decpro.entity.DecisionLine;
import com.warwick.analytics.decpro.entity.FileStatus;
import com.warwick.analytics.decpro.entity.Range;

import javax.batch.api.Batchlet;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

import static java.util.logging.Logger.getLogger;

/**
 * @author Marco Ribeiro
 */
@Named
public class FilteringFileBatchlet extends AbstractInputFileProcess implements Batchlet {


    @Inject
    private DecisionBusiness decisionBusiness;

    @PersistenceContext
    protected EntityManager em;

    @Override
    public String process() throws Exception {
        query();
        return "COMPLETED";
    }

    @Override
    public void stop() throws Exception {
    }

    public void query() throws Exception {
        Query query = em.createQuery(
                "select d" +
                        " FROM DecisionLine d " +
                        " WHERE d.decisionFile.id = :dFileId AND d.decision = :decisionBol");
        query.setParameter("dFileId", getContext().getFileToProcess().getId());
        query.setParameter("decisionBol", true);
        List resultList = query.getResultList();
        if (resultList.isEmpty()) {
            return;
        }
        Map<Integer, Collection<Integer>> varValues = new TreeMap<>();
        for (Object obj : resultList) {
            DecisionLine dl = (DecisionLine) obj;
            for (int i = 0; i < dl.getVariables().length; i++) {
                if (varValues.get(i + 1) == null) {
                    varValues.put(i + 1, new ArrayList<>());
                }
                varValues.get(i + 1).add(dl.getVariables()[i]);
            }
        }
        Map<Integer, Range> varMinMax = new TreeMap<>();
        for (Integer integer : varValues.keySet()) {
            varMinMax.put(integer, new Range(Collections.min(varValues.get(integer)), Collections.max(varValues.get(integer))));
        }

        filter(varMinMax);
        updateFile();
    }

    private void updateFile() throws Exception {
        DecisionFile decisionFile = getContext().getFileToProcess();

        decisionFile.setFileStatus(FileStatus.DONE);
        decisionBusiness.updateDecisionFile(decisionFile);
    }

    private void filter(Map<Integer, Range> intervalMap) {
        //get decisions where decision == 0 (false)
        Query query = em.createQuery(
                "select d" +
                        " FROM DecisionLine d " +
                        " WHERE d.decisionFile.id = :dFileId AND d.decision = :decisionBol");
        query.setParameter("dFileId", getContext().getFileToProcess().getId());
        query.setParameter("decisionBol", false);
        List decisionLines = query.getResultList();


        List<DecisionLine> toRemove = new ArrayList<>();
        for (Object obj : decisionLines) {
            DecisionLine decisionLine = (DecisionLine) obj;
            boolean remove = true;

            for (int i = 0; i < decisionLine.getVariables().length; i++) {
                if (insideRange(decisionLine.getVariables()[i], intervalMap.get(i + 1))) {
                    remove = false;
                    break;
                }
            }

            if (remove)
                toRemove.add(decisionLine);
        }
        for (DecisionLine dlTr : toRemove) {
            decisionBusiness.removeDecisionLine(dlTr.getId());
        }
        getLogger(this.getClass().getName()).log(Level.FINE, "Removed " + toRemove.size() + " rows from file " + getContext().getFileToProcess());
    }

    private boolean insideRange(int value, Range range) {
        return value >= range.getMin() && value <= range.getMax();
    }


}