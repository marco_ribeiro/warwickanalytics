package com.warwick.analytics.decpro.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

/**
 * @author Marco Ribeiro
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
        @NamedQuery(name = "DecisionLine.findByFileId",
                query = "SELECT dl FROM DecisionLine dl " +
                        "WHERE dl.decisionFile.id = :id " +
                        "ORDER BY dl.decisionId ASC"),
        @NamedQuery(name = "DecisionLine.listAll",
                query = "SELECT dl FROM DecisionLine dl"),
        @NamedQuery(name = "DecisionLine.findById",
                query = "SELECT dl FROM DecisionLine dl "+
                        "WHERE dl.id = :id"),
})
public class DecisionLine implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "decisionLineId")
    @SequenceGenerator(name = "decisionLineId",
            sequenceName = "DECISIONLINE_ID_SEQ",
            initialValue = 1,
            allocationSize = 1)
    private Long id;

    @Id
    @ManyToOne
    private DecisionFile decisionFile;

    private Long decisionId;

    private Integer[] variables;

    private boolean decision;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDecisionId() {
        return decisionId;
    }

    public void setDecisionId(Long decisionId) {
        this.decisionId = decisionId;
    }

    public DecisionFile getDecisionFile() {
        return decisionFile;
    }

    public void setDecisionFile(DecisionFile decisionFile) {
        this.decisionFile = decisionFile;
    }

    public Integer[] getVariables() {
        return variables;
    }

    public void setVariables(Integer[] variables) {
        this.variables = variables;
    }

    public boolean isDecision() {
        return decision;
    }

    public void setDecision(boolean decision) {
        this.decision = decision;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DecisionLine decisionLine = (DecisionLine) o;

        return decisionFile.equals(decisionLine.decisionFile) && decisionId.equals(decisionLine.decisionId);
    }

    @Override
    public int hashCode() {
        int result = decisionId.hashCode();
        result = 31 * result + decisionFile.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "DecisionLine{" +
                "id=" + id +
                "decisionId=" + decisionId +
                ", decisionFile=" + decisionFile +
                ", variables=" + variables +
                ", decision=" + decision +
                '}';
    }
}