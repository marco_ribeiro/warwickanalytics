package com.warwick.analytics.decpro.entity;

/**
 * @author Marco Ribeiro
 */
public enum FolderType {
    FI, FO, FP, FI_TMP, FO_TMP
}
