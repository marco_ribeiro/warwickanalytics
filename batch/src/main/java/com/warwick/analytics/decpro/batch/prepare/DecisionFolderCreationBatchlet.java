package com.warwick.analytics.decpro.batch.prepare;

import com.warwick.analytics.decpro.business.DecisionBusiness;
import com.warwick.analytics.decpro.configuration.Configuration;
import com.warwick.analytics.decpro.entity.DecisionFolder;
import com.warwick.analytics.decpro.entity.FolderType;
import org.apache.commons.io.FileUtils;

import javax.batch.api.AbstractBatchlet;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import static java.util.logging.Logger.getLogger;

/**
 * @author Marco Ribeiro
 */
@Named
public class DecisionFolderCreationBatchlet extends AbstractBatchlet {
    @Inject
    private DecisionBusiness decisionBusiness;

    @Inject
    @Configuration
    private String batchHome;
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    @Inject
    @Configuration
    private Map<String, List<FolderType>> folders;

    @Override
    public String process() throws Exception {
        getLogger(this.getClass().getName()).log(Level.INFO, this.getClass().getSimpleName() + " running");

        folders
                .forEach((folderRoot, folderTypes) -> folderTypes
                                .forEach(folderType ->
                                        verifyAndCreateFolder(folderRoot, folderType)));

        getLogger(this.getClass().getName()).log(Level.INFO, this.getClass().getSimpleName() + " completed");
        return "COMPLETED";
    }

    private void verifyAndCreateFolder(String folderRoot, FolderType folderType){
        File folder = new File(
                batchHome + "/" + "/" + folderType);

        if (!folder.exists()) {
            try {
                getLogger(this.getClass().getName()).log(Level.INFO, "Creating folder " + folder);
                FileUtils.forceMkdir(folder);
                decisionBusiness.createDecisionFolder(new DecisionFolder(folderType, folder.getPath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (decisionBusiness.findDecisionFolderById(folderType) == null) {
            decisionBusiness.createDecisionFolder(new DecisionFolder(folderType, folder.getPath()));
        }
    }

}
