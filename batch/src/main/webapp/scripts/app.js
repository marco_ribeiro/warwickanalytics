var app = angular.module('app', ['ngResource', 'ngGrid', 'ui.bootstrap', 'ngFileUpload']);

app.controller('linesController', function ($scope, $filter, decisionsService, $rootScope) {
    $rootScope.$on('refreshLinesGrid', function (event, id) {
        decisionsService.query({fileId: id}, function (data) {
            $scope.decisionLines = data;
            if ($scope.decisionLines.length > 0) {
                numberVars = $scope.decisionLines[0].variables.length;
                var copyColDefs = [
                    {field: 'decisionId', displayName: 'Id'},
                    {field: 'decision', displayName: 'Decision', cellTemplate: '<span>{{COL_FIELD | toBinary}}</span>'},
                ];
                var decisionCol = copyColDefs.pop();
                for (var i = 0; i < numberVars; i++) {
                    var newCol = {field: 'variables[' + i + ']', displayName: 'Var' + (i + 1)}
                    copyColDefs.push(newCol);
                }
                copyColDefs.push(decisionCol);
                $scope.defaultColumnDefs = copyColDefs;

            }
        })
    });
    $scope.submit = function () {
        $rootScope.$broadcast('refreshLinesGrid', $scope.searchId);
    };

    $scope.defaultColumnDefs = [
        {field: 'decisionId', displayName: 'Id'},
        {field: 'decision', displayName: 'Decision', cellTemplate: '<span>{{COL_FIELD | toBinary}}</span>'},
    ];

    $scope.toBinary = function(row){
        if(row.entity.decision === true){
            return 1;
        }else {
            return 0;
        }
    }

    $scope.gridOptions = {
        data: 'decisionLines',
        useExternalSorting: false,
        multiSelect: false,
        selectedItems: [],
        columnDefs: 'defaultColumnDefs'
    };
});

app.controller('filesController', function ($scope, $filter, filesService, $rootScope) {
    $rootScope.$on('refreshFilesGrid', function (event) {
        filesService.query(function (data) {
            $scope.decisionFiles = data;
            $rootScope.decisionFilesLength = $scope.decisionFiles.length;
        })
    });
    $scope.submit = function () {
        $rootScope.$broadcast('refreshFilesGrid');
    };

    $scope.seeDetails = function (dfRow) {
        $rootScope.$broadcast('refreshLinesGrid', dfRow.entity.id);
    };

    $scope.gridOptions = {
        data: 'decisionFiles',
        useExternalSorting: false,
        multiSelect: false,
        selectedItems: [],

        columnDefs: [
            {
                field: 'id', displayName: 'Id',
                cellTemplate: '<a href="" ng-click="seeDetails(row)">{{COL_FIELD}}</a>'
            },
            {field: 'fileName', displayName: 'File Name'},
            {field: 'fileStatus', displayName: 'Status'},

        ]
    };
});

app.controller('uploaderController', function ($scope, fileUpload, $rootScope,$timeout) {
    $scope.uploadFile = function (file) {
        var uploadUrl = "resources/decisions/upload";
        $scope.uploadSuccess = false;
        $scope.errorMsg = false;
        fileUpload.uploadFileToUrl(file, uploadUrl,
            function(){
                $scope.uploadSuccess = true;
                var dLlength = $rootScope.decisionFilesLength;
                if(typeof dLlength === 'undefined'){
                    dLlength = 0;
                }
                $scope.timerRefreshFiles = setInterval(function(){
                    $rootScope.$broadcast("refreshFilesGrid");
                    if(dLlength<$rootScope.decisionFilesLength){
                        clearInterval($scope.timerRefreshFiles);
                    }
                }, 1000);
                $timeout(function(){
                    $scope.uploadSuccess = false;
                }, 2000);
            },
            function(){
                $scope.errorMsg = true;
                $timeout(function(){
                    $scope.errorMsg = false;
                }, 2000);
            });

    };
});

app.factory('decisionsService', function ($resource) {
    return $resource('resources/decisions/lines');
});

app.factory('filesService', function ($resource) {
    return $resource('resources/decisions/files');
});

app.service('fileUpload', function ($http) {
    this.uploadFileToUrl = function (file, uploadUrl, successCallback, errorCallback) {
        $http.post(uploadUrl, file, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .success(function () {
                angular.element("input[type='file']").val(null);
                successCallback();
            })
            .error(function () {
                errorCallback();
            });
    }
});

app.filter('toBinary', function () {
   return function(input){
       return input ? '1' : '0';
   }
});

