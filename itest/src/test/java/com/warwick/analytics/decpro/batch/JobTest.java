package com.warwick.analytics.decpro.batch;

import com.warwick.analytics.decpro.batch.util.BatchTestHelper;
import com.warwick.analytics.decpro.business.DecisionBusiness;
import com.warwick.analytics.decpro.entity.DecisionFolder;
import com.warwick.analytics.decpro.entity.FolderType;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.BatchStatus;
import javax.batch.runtime.JobExecution;
import javax.inject.Inject;
import java.io.File;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * @author Marco Ribeiro
 */
@SuppressWarnings({"unchecked", "CdiInjectionPointsInspection"})
@RunWith(Arquillian.class)
public class JobTest {

    @Inject
    private DecisionBusiness decisionBusiness;


    @Deployment
    public static WebArchive createDeployment() {
        File[] requiredLibraries = Maven.resolver().loadPomFromFile("pom.xml")
                                        .resolve("commons-io:commons-io", "commons-dbutils:commons-dbutils")
                                        .withTransitivity().asFile();

        WebArchive war = ShrinkWrap.create(WebArchive.class)
                                   .addAsLibraries(requiredLibraries)
                                   .addPackages(true, "com.warwick.analytics.decpro")
                                   .addAsWebInfResource("META-INF/beans.xml")
                                   .addAsResource("META-INF/persistence.xml")
                                   .addAsResource("META-INF/batch-jobs/prepare-job.xml");
        System.out.println(war.toString(true));
        return war;
    }


    @Test
    @InSequence(1)
    public void testPrepareJob() throws Exception {
        JobOperator jobOperator = BatchRuntime.getJobOperator();
        Long executionId = jobOperator.start("prepare-job", new Properties());

        JobExecution jobExecution = BatchTestHelper.keepTestAlive(jobOperator, executionId);

        DecisionFolder decisionFolderById = decisionBusiness.findDecisionFolderById(FolderType.FI);

        assertFalse(decisionFolderById == null);

        assertEquals(BatchStatus.COMPLETED, jobExecution.getBatchStatus());
    }
}
